# helper class for providing a central switch for logging and debug messages
import inspect

# TODO Logger Level
# 0: Log everything
# 1: Log everything but is_field_on_board and is_field_empty
# ...
#


# Source:
# https://stackoverflow.com/questions/17065086/how-to-get-the-caller-class-name-inside-a-function-of-another-class-in-python
# removed - did not work as intended
def get_class_from_frame(fr):
    return ""


class Logger:
    def __init__(self, log_level):
        self.log_level = log_level

    def p(self, msg, x=False):
        if self.log_level > 0:
            stack = inspect.stack()
            #_class = stack[1][0].f_locals["self"].__class__.__name__
            #_class = list(stack[1][0].f_locals.keys())[0]
            _class = get_class_from_frame(stack[1][0])
            _method = stack[1][0].f_code.co_name
            print("{}.{}(): {}".format(_class, _method, msg))
            #print(".{}(): {}".format(_method, msg))
            if x:
                print("---")

    # print message plus separator (visual purposes only)
    def px(self, msg):
        self.p(msg, x=True)
