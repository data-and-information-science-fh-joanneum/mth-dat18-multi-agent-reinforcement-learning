# class for configuring and tweaking settings
import torch


class Config:
    # set logger level. for now only 0 or 1 are supported, with 0 meaning no debug messages are shown
    # in the console and 1 meaning all debug messages are shown
    logger_lvl = 0

    # dimensions of playfield - default nine, minimum seven (for 4v4). developed with odd numbers in mind
    pf_dim = 9

    # generate obstacles on playfield
    pf_obs = False

    # display playfield every x modulo rounds
    display_pf_mod = 1

    # time in seconds to pause log output for better readability
    log_pause = 1

    # number of matches played for balancing
    num_matches = 10000

    # points of damaged added to critical hits
    crit_modifier = 2

    # punishment for failed move
    punish_move = -1

    # punishment for hitting friendly
    punish_hit_friendly = -1

    # punishment for dying
    punish_died = -10

    # punishment for failed attack of melee fighter
    punish_att_fail_mf = -1

    # reward for hitting enemy
    reward_hit_enemy = 2

    # reward for killing enemy
    reward_kill = 20

    # no armour check
    skip_armour_chk = True

    # allow critical hits
    allow_crit_hit = False

    ### DQN Settings ###

    dqn_GAMMA = 0.99
    dqn_BATCH_SIZE = 32
    dqn_REPLAY_SIZE = 100000
    #dqn_REPLAY_SIZE = 100
    dqn_LEARNING_RATE = 1e-4
    dqn_SYNC_TARGET_FRAMES = 20
    dqn_REPLAY_START_SIZE = 1000

    #dqn_EPSILON_DECAY_LAST_FRAME = 150000
    dqn_EPSILON_DECAY_LAST_FRAME = 500000
    dqn_EPSILON_START = 0.99
    dqn_EPSILON_FINAL = 0.05
    """
    dqn_EPSILON_DECAY_LAST_FRAME = 1500
    dqn_EPSILON_START = 0.8
    dqn_EPSILON_FINAL = 0.05
    """

    # FULLY OBSERVABLE
    # not available for all scenarios
    fully_observable = True

    ### Cuda Settings ###
    # only cuda support for now
    device = torch.device("cuda")
