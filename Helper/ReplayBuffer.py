#####
# IMPORTANT LICENSE NOTICE
# CODE IN THIS FILE BASED ON
# https://github.com/PacktPublishing/Deep-Reinforcement-Learning-Hands-On
# WHICH IS LICENSED UNDER THE MIT-LICENSE
# https://github.com/PacktPublishing/Deep-Reinforcement-Learning-Hands-On/blob/master/LICENSE
#####

import collections
import numpy as np


class ReplayBuffer:
    def __init__(self, capacity):
        self.buffer = collections.deque(maxlen=capacity)

    def __len__(self):
        return len(self.buffer)

    def append(self, experience):
        self.buffer.append(experience)

    def sample(self, batch_size):
        indices = np.random.choice(len(self.buffer), batch_size,
                                   replace=False)
        states, actions, rewards, dones, next_states = \
            zip(*[self.buffer[idx] for idx in indices])
        return np.array(states), np.array(actions), \
               np.array(rewards, dtype=np.float32), \
               np.array(dones, dtype=np.uint8), \
               np.array(next_states)
