#####
# IMPORTANT LICENSE NOTICE
# CODE IN THIS FILE BASED ON
# https://github.com/PacktPublishing/Deep-Reinforcement-Learning-Hands-On
# WHICH IS LICENSED UNDER THE MIT-LICENSE
# https://github.com/PacktPublishing/Deep-Reinforcement-Learning-Hands-On/blob/master/LICENSE
#####

import torch
import torch.nn as nn
import numpy as np
from Helper.Config import Config


class DQN(nn.Module):
    def __init__(self, input_shape, n_actions):
        super(DQN, self).__init__()

        self.conv = nn.Sequential(
            nn.Conv2d(input_shape[0], 32, kernel_size=2, stride=1),
            nn.ReLU(),
            nn.Conv2d(32, 64, kernel_size=2, stride=1),
            nn.ReLU()
        )

        conv_out_size = self._get_conv_out(input_shape)
        self.fc = nn.Sequential(
            nn.Linear(conv_out_size, 512),
            nn.ReLU(),
            nn.Linear(512, n_actions)
        )

    def _get_conv_out(self, shape):
        o = self.conv(torch.zeros(1, *shape))
        return int(np.prod(o.size()))

    def forward(self, x):
        conv_out = self.conv(x).view(x.size()[0], -1)
        return self.fc(conv_out)

    def calc_loss(batch, net, tgt_net, device="cuda"):
        states, actions, rewards, dones, next_states = batch

        states_v = torch.tensor(np.array(states, copy=False)).type(torch.cuda.FloatTensor).to(device)
        next_states_v = torch.tensor(np.array(next_states, copy=False)).type(torch.cuda.FloatTensor).to(device)
        actions_v = torch.tensor(actions).to(device)
        rewards_v = torch.tensor(rewards).to(device)
        done_mask = torch.BoolTensor(dones).to(device)

        state_action_values = net(states_v).gather(1, actions_v.unsqueeze(-1)).squeeze(-1)
        with torch.no_grad():
            next_state_values = tgt_net(next_states_v).max(1)[0]
            next_state_values[done_mask] = 0.0
            next_state_values = next_state_values.detach()

        expected_state_action_values = next_state_values * Config.dqn_GAMMA + rewards_v
        return nn.MSELoss()(state_action_values, expected_state_action_values)

    def calc_epsilon(config, iteration):
        e = max(config.dqn_EPSILON_FINAL,
                   config.dqn_EPSILON_START - iteration / config.dqn_EPSILON_DECAY_LAST_FRAME)
        return e

    def step(scenario, agent):
        scenario.tgt_net[agent.class_name].load_state_dict(scenario.net[agent.class_name].state_dict())
        scenario.optimizer[agent.class_name].zero_grad()
        batch = scenario.buffer[agent.class_name].sample(scenario.cfg.dqn_BATCH_SIZE)
        loss_t = DQN.calc_loss(batch, scenario.net[agent.class_name], scenario.tgt_net[agent.class_name],
                               device=scenario.cfg.device)
        loss_t.backward()
        scenario.optimizer[agent.class_name].step()
