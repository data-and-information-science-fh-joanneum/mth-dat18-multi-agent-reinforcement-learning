import time
import json

# GIF generation
from PIL import Image
import os

# result plotting
import matplotlib.pyplot as plt

from Environment import Environment
from Helper.DQN import DQN
from Helper.Experience import Experience

# DUMMY ENV:
# one hero with DQN vs one randomly acting goblin
# from Scenarios.DummyEnv import DummyEnv

# ONE V ONE:
# one hero vs one goblin, both have DQNs
# from Scenarios.OneVsOneEnv import OneVsOneEnv

# ONLY GOBLIN ENV:
# reverse DUMMY ENV
# from Scenarios.OnlyGoblinEnv import OnlyGoblinEnv

# GOB VS GOB
# one goblin vs one goblin, both have DQNs
# from Scenarios.GobVsGobEnv import GobVsGobEnv

# GOB2 VS GOB2
# two goblin vs two goblin, all have DQNs
from Scenarios.Gob2VsGob2Env import Gob2VsGob2Env

# CURRENT SCENARIO
scenario = Gob2VsGob2Env()
# USE CONFIG OF SCENARIO
cfg = scenario.cfg

# HELPER DATA STRUCTURES FOR PERSISTING DATA
reward_dict = {}
for char in scenario.reward_dict:
    reward_dict[char] = []

persist_data_dict = {'episode_nr': [],
                     'turn_count': [],
                     'current_epsilon': [],
                     'reward_dict': reward_dict,
                     'win_count': {
                         'heroes': [],
                         'goblins': []
                        }
                     }

training_id = str(time.time())
print(training_id)
render_episode_list = []


def save_reward_and_exp(char, obs, done):
    if char.class_name in scenario.agents_with_dqn:
        char.rewards.append(char.reward_last_round)
        old_state = char.observation_last_round.astype('float32')
        new_state = obs.astype('float32')
        exp = Experience(old_state, char.action_last_round, char.reward_last_round, done, new_state)
        scenario.buffer[char.class_name].append(exp)


def handle_defeated(char, env):
    if not char.more_than_one_round_dead:
        char.reward_last_round += cfg.punish_died
        save_reward_and_exp(char=char, obs=env.get_observation(char), done=True)
        char.more_than_one_round_dead = True


def is_match_over(env, scenario, char):
    if len(env.goblins) != 0 and len(env.heroes) != 0:
        return False
    if len(env.goblins) == 0:
        env.logger.p("~~~ HEROES WON ~~~")
    else:
        env.logger.p("~~~ GOBLINS WON ~~~")
    scenario.team_win_count[char.team] += 1
    char.reward_last_round += cfg.reward_kill
    save_reward_and_exp(char=char, obs=env.get_observation(char), done=True)
    return True


def generate_persist_data():
    persist_data_dict['episode_nr'].append(len(scenario.round_count_list))
    persist_data_dict['turn_count'].append(scenario.round_count_list[-1])
    persist_data_dict['current_epsilon'].append(DQN.calc_epsilon(config=cfg, iteration=sum(scenario.round_count_list)))
    persist_data_dict['win_count']['heroes'].append(scenario.team_win_count[1])
    persist_data_dict['win_count']['goblins'].append(scenario.team_win_count[2])

    for char in scenario.reward_dict:
        persist_data_dict['reward_dict'][char].append(scenario.reward_dict[char][-1])


def save_persisted_data():
    with open('../MARL/JSON/' + scenario.scenario_name + "_" + training_id + '.json', 'w') as fout:
        json.dump(persist_data_dict, fout)

    # with open(r"../MARL/JSON/test.json", "r") as read_file:
        # data = json.load(read_file)


def draw_playfield(round_count):
    if round_count % cfg.display_pf_mod == 0 and cfg.logger_lvl > 0:
        env.print_pf()
        time.sleep(cfg.log_pause)


def draw_reward_win_graph():
    plt.cla()
    plt.plot(persist_data_dict['win_count']['heroes'])
    plt.plot(persist_data_dict['win_count']['goblins'])
    plt.show()

    plt.cla()
    for char in scenario.reward_dict:
        plt.plot(persist_data_dict['reward_dict'][char])
    plt.show()


def render_gif():
    png_dir = '../MARL/GIF/'
    png_list = os.listdir(png_dir)

    # collect list of files generated this training
    training_files = []
    for file_name in png_list:
        if training_id in file_name:
            training_files.append(file_name)

    if len(training_files) == 0:
        print("No pictures matching training found, aborting GIF creation")
        return

    # match files with training episodes
    render_episodes = {}
    for episode in render_episode_list:
        render_episodes[episode] = []
        for file_name in training_files:
            if "_" + episode + "_" in file_name:
                render_episodes[episode].append(file_name)

    # render gif for every recorded episode
    for render_episode in render_episodes:
        frames = []
        render_episodes[render_episode].sort()
        for frame in render_episodes[render_episode]:
            new_frame = Image.open(png_dir + frame)
            frames.append(new_frame)
        gif_file_name = scenario.scenario_name + "_" + render_episodes[render_episode][0][:-10] + ".gif"
        frames[0].save(png_dir + "compiled/" + gif_file_name, format='GIF',
                       append_images=frames[1:],
                       save_all=True,
                       duration=300, loop=0)


def play_one_episode(env, max_rounds, training_id):
    # SETUP OF EPISODE
    episode_id = len(scenario.round_count_list)
    GIF_id = training_id + "_" + str(episode_id) + "_"
    round_count = 0
    env.create_agents(scenario)
    done = False

    while round_count <= max_rounds and not done:
        # ts_start_of_round = time.time()
        # print(ts_start_of_round)
        round_count += 1
        env.logger.p("*** round " + str(round_count))
        # print(round_count)

        # DECAYING EPSILON
        epsilon = DQN.calc_epsilon(config=cfg, iteration=sum(scenario.round_count_list) + round_count)
        # print(str(round_count) + "-" + str(epsilon))

        # ITERATE OVER AGENTS
        for char in env.agents:
            # ts_start_of_char = time.time()
            # HANDLE DEFEATED
            if char.defeated:
                handle_defeated(char, env)
                continue

            # RESET REWARD
            env.turn_reward = 0

            # ts_before_obs = time.time()

            env.logger.p("### " + str(char.ID) + "'s turn.")
            # REQUEST OBS FROM ENV
            observation = env.get_observation(char)

            if char.observation_last_round is not None:
                save_reward_and_exp(char=char, obs=observation, done=False)

            # SAVE POSITION BEFORE ACTION
            pos_before_action = (char.x, char.y)

            # SELECT NEW ACTION
            if char.class_name in scenario.agents_with_dqn:
                action = char.choose_dqn_action(observation, epsilon, cfg, scenario.net[char.class_name])
            else:
                action = char.choose_random_action()

            # GENERATE PICTURE
            render = episode_id % 2000 == 0
            if render and str(episode_id) not in render_episode_list:
                render_episode_list.append(str(episode_id))
            env.generate_picture(char, pos_before_action, action, round_count, render, GIF_id, save_PNGs=True)

            # SAVE OLD REWARDS ETC
            char.reward_last_round = env.turn_reward
            char.observation_last_round = observation
            char.action_last_round = action

            if is_match_over(env, scenario, char):
                done = True
                break

    scenario.round_count_list.append(round_count)


# START OF LOGIC
# for x in range(scenario.cfg.num_matches):
for x in range(10000):

    # CREATE ENVIRONMENT
    env = Environment(size=cfg.pf_dim, obstacles=cfg.pf_obs, logger_lvl=cfg.logger_lvl)
    # PLAY ONE EPISODE
    play_one_episode(env, max_rounds=100, training_id=training_id)
    print("episode: " + str(len(scenario.round_count_list)) + ", " + "turns: " + str(sum(scenario.round_count_list)))
    scenario.save_rewards(env)
    print("win count: heroes - " + str(scenario.team_win_count[1]) + ", goblins - " + str(scenario.team_win_count[2]))

    generate_persist_data()

    if x > 3:  # train DQN after 3rd episode
        for agent in env.agents:
            if agent.class_name in scenario.agents_with_dqn:
                DQN.step(scenario, agent)


render_gif()
draw_reward_win_graph()
save_persisted_data()
print(training_id)

end = True
