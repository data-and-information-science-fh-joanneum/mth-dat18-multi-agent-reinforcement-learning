import random
import numpy as np

import matplotlib.patches as mpatches
from matplotlib import pyplot as plt

from Agents.GoblinDummyAgent import GoblinDummyAgent
from Agents.HeroDummyAgent import HeroDummyAgent
from Agents.Enemies.Goblin import Goblin
from Agents.Enemies.Goblin_ii import Goblin_ii
from Agents.Heroes.Warrior import Warrior
from Agents.Heroes.GoodGoblin import GoodGoblin
from Agents.Heroes.GoodGoblin_ii import GoodGoblin_ii
from Helper.Logger import Logger
from Helper.Config import Config

# colors
white = [255, 255, 255]
black = [0, 0, 0]
red = [255, 0, 0]
green = [0, 255, 0]
blue = [0, 0, 255]
orange = [255, 165, 0]
yellow = [255, 255, 0]
grey = [165, 165, 165]

# color mapping
c_empty = white
c_obstacle = grey
c_team_1 = blue
c_team_2 = red
c_action_move = green
c_action_attack = orange


# playfield is the central class that keeps track of chars and obstacles, as well as providing helper functions
class Environment:
    def __init__(self, size, obstacles, logger_lvl):
        # set logger helper class for debugging
        self.logger = Logger(logger_lvl)
        # size of playfield, must be at least seven.
        """if size < 7:
            raise Exception('Size of playfield must be seven or more')"""
        self.size = size
        # initialize playfield with None. None means nothing is on the field
        self.board = [[None for i in range(size)] for j in range(size)]
        # list of characters
        self.agents = []
        # list of obstacles
        self.obstacles = []
        # list of heroes
        self.heroes = []
        # list of goblins
        self.goblins = []
        # create obstacles at random places, where chars can not move
        if obstacles:
            self.set_obstacles()
        # cumulative reward for the current turn
        self.turn_reward = 0
        self.field_blueprint = self.generate_blueprint()
        self.picture_counter = 0

    # counter for giving unique IDs to chars
    id_counter = 1

    ### HELPER FUNCTIONS ###

    def generate_blueprint(self):
        field = np.zeros((self.size, self.size, 3), dtype=np.uint8)

        # iterate over rows
        for i in range(self.size):
            # iterate over columns
            for j in range(self.size):
                # check if field is empty
                if self.board[i][j] is None:
                    field[i][j] = c_empty
                # check if field is an obstacle
                elif self.board[i][j] == "x":
                    field[i][j] = c_obstacle

        # horizontal boarders
        field = np.insert(field, self.size, np.zeros((self.size, 3), dtype=np.uint8), 0)
        field = np.insert(field, 0, np.zeros((self.size, 3), dtype=np.uint8), 0)
        # vertical boarders
        field = np.insert(field, self.size, np.zeros((1, self.size + 2, 3), dtype=np.uint8), 1)
        field = np.insert(field, 0, np.zeros((1, self.size + 2, 3), dtype=np.uint8), 1)

        return field

    # generate picture of playfield
    def generate_picture(self, char, pos_before_action, action, round_count, render, GIF_id, save_PNGs):

        # load blueprint

        field = np.array(self.field_blueprint, copy=True)

        # add agents
        # (+1 to account for boarder)
        for agent in self.agents:
            if agent.defeated:
                continue
            if agent.team == 1:
                field[agent.x+1][agent.y+1] = c_team_1
            if agent.team == 2:
                field[agent.x+1][agent.y+1] = c_team_2

        # add action draw
        # (+1 to account for boarder)
        x = pos_before_action[0] + 1
        y = pos_before_action[1] + 1

        # move_north
        if action == 0: field[x-1][y] = c_action_move
        # move_east
        if action == 1: field[x][y+1] = c_action_move
        # move_south
        if action == 2: field[x+1][y] = c_action_move
        # move_west
        if action == 3: field[x][y-1] = c_action_move
        # action_north
        if action == 4: field[x-1][y] = c_action_attack
        # action_east
        if action == 5: field[x][y+1] = c_action_attack
        # action_south
        if action == 6: field[x+1][y] = c_action_attack
        # action_west
        if action == 7: field[x][y-1] = c_action_attack

        # add old position of agent
        # if agent moved, new position was overwritten by move color
        if char.team == 1:
            field[x][y] = c_team_1
        if char.team == 2:
            field[x][y] = c_team_2

        if render:
            # clear old image
            plt.cla()
            plt.imshow(field)
            red_patch = mpatches.Patch(color='black', label="round:" + str(round_count))
            # TODO add rewards, hp
            plt.legend(handles=[red_patch], bbox_to_anchor=(1.05, 1), loc='upper left')
            plt.tight_layout(rect=[0, 0, 0.75, 1])
            #plt.show(block=False)
            # TODO path to config
            if save_PNGs:
                plt.savefig('../MARL/GIF/' + GIF_id + str(self.picture_counter).zfill(5) + ".png")
            self.picture_counter += 1
            #plt.pause(0.05)

        return plt

    # function to nicely print playfield
    def print_pf(self):
        # print upper boarder
        print("#" * ((self.size * 2) + 2))
        # iterate over rows
        for i in range(self.size):
            # print left boarder
            print("#|", end='')
            # iterate over columns
            for j in range(self.size):
                # check if field is empty
                if self.board[i][j] is None:
                    print("_|", end='')
                # check if field is an obstacle
                elif self.board[i][j] == "x":
                    print("x|", end='')
                # check if field is occupied by char
                else:
                    print(str(self.board[i][j].ID) + "|", end='')
            # print right boarder
            print("#")
        # print lower boarder
        print("#" * ((self.size * 2) + 2))

    # function to check if field is on board
    def is_field_on_board(self, x, y):
        if (x >= 0) and (x < self.size) and (y >= 0) and (y < self.size):
            #self.logger.p("field on board")
            return True
        else:
            #self.logger.p("field not on board")
            return False

    # function to check if field is empty. otherwise a field would be occupied by a char or an obstacle
    def is_field_empty(self, x, y):
        if self.board[x][y] is None:
            #self.logger.p("field empty")
            return True
        else:
            #self.logger.p("field not empty")
            return False

    # function to check if two chars are in the same team
    def is_ally(self, char, target):
        if char.team == target.team:
            self.logger.p("char and target are allies")
            return True
        else:
            self.logger.p("char and target are enemies")
            return False

    # function to create obstacles at random places, where chars can not move
    def set_obstacles(self):
        # random number to determine how many obstacles will be created. this is done to bring variety to the
        # playfield. the range was defined to make a complete separation of the playfield impossible, as the
        # number of obstacles must be smaller than the width of the playfield.
        num_obs = random.randint(round(self.size / 2), self.size - 1)
        self.logger.p("number of obstacles: " + str(num_obs))
        for i in range(num_obs):
            # bool for avoiding to place two obstacles on the same field
            found_new_spot = False
            # loop to make sure an empty field is found
            while not found_new_spot:
                # picks a random row for a new obstacle. it is designed to leave the first and the second to the
                # top rows, as well as the last and second to last rows empty, avoiding a conflict with the start
                # areas of the chars
                x_cor = random.randint(0 + 2, self.size - 3)
                # picks a random column for a new obstacle
                y_cor = random.randint(0, self.size - 1)
                # sets obstacle if field is empty
                if self.is_field_empty(x_cor, y_cor):
                    self.board[x_cor][y_cor] = "x"
                    self.obstacles.append((x_cor, y_cor))
                    found_new_spot = True

    # function to check if field is occupied by obstacle.
    def is_obstacle(self, x, y):
        if self.board[x][y] is "x":
            self.logger.p("field is obstacle")
            return True
        else:
            self.logger.p("field is not obstacle")
            return False

    ### SETTING AND REMOVING ###

    # function for initializing a char
    def set_char(self, char):
        # sanity check if field is on board and empty
        if self.is_field_on_board(char.x, char.y) and self.is_field_empty(char.x, char.y):
            # set char to designated field
            self.board[char.x][char.y] = char
            self.logger.p("placing char successful")
            return True
        else:
            self.logger.p("placing char failed")
            return False

    # function for removing a char
    def remove_char(self, char):
        # remove from char list
        '''self.characters.remove(char)
        # remove from goblins or heroes list'''
        if char.team == 1:
            self.heroes.remove(char)
        else:
            self.goblins.remove(char)
        # mark field as empty
        self.board[char.x][char.y] = None
        # set position secluded from playfield to avoid errors
        char.x = -10
        char.y = -10
        # mark char as defeated
        char.defeated = True
        self.logger.p(str(char.ID) + " removed from game")

    ### MOVING ###

    # base function to move a char
    def __move(self, char, dir, target_x, target_y):
        self.logger.p(str(char.ID) + " move " + dir)
        # check if target position is valid
        if not self.is_field_on_board(target_x, target_y):
            # moving was not successful
            success = False
        elif not self.is_field_empty(target_x, target_y):
            # moving was not successful
            success = False
        else:
            # moving was successful
            success = True
            # mark old position as empty
            self.board[char.x][char.y] = None
            # mark new position as occupied
            self.board[target_x][target_y] = char
            char.x = target_x
            char.y = target_y
        if success:
            self.logger.px("moved successfully")
        else:
            self.logger.px("moving failed")
        # return if moving was successful or not
        return success

    # functions to move char in certain direction
    def move_north(self, char):
        return self.__move(char, "north", char.x - 1, char.y)

    def move_south(self, char):
        return self.__move(char, "south", char.x + 1, char.y)

    def move_east(self, char):
        return self.__move(char, "east", char.x, char.y + 1)

    def move_west(self, char):
        return self.__move(char, "west", char.x, char.y - 1)

    ### FIELD OF VIEW ###
    #TODO
    # - obstacles
    # - different functions for class-indentification

    # function to provide the field of view for a char which is needed for the reinforcement algorithm.
    # returns a square 2d array with the char in the middle

    def get_observation(self, char, dist=2):
        full_state = Config.fully_observable
        if full_state:
            full_state = self.get_full_state(char)
            return full_state
        else:
            obs = [np.array(self.__view_field_on_board(char, dist)), np.array(self.__view_enemy(char, dist))]
            return np.asarray(obs)

    def __view_field_on_board(self, char, dist):
        view = [[None for i in range((dist * 2) + 1)] for j in range((dist * 2) + 1)]
        i = 0
        for x in range(char.x - dist, char.x + dist + 1):
            j = 0
            for y in range(char.y - dist, char.y + dist + 1):
                if not self.is_field_on_board(x, y):
                    view[i][j] = 0
                else:
                    view[i][j] = 1
                j = j + 1
            i = i + 1
        return view

    def __view_enemy(self, char, dist):
        view = [[None for i in range((dist * 2) + 1)] for j in range((dist * 2) + 1)]
        i = 0
        for x in range(char.x - dist, char.x + dist + 1):
            j = 0
            for y in range(char.y - dist, char.y + dist + 1):
                view[i][j] = 0
                if self.is_field_on_board(x, y):
                    if not self.is_field_empty(x, y):
                        if not self.is_obstacle(x, y):
                            if not self.is_ally(char, self.board[x][y]):
                                view[i][j] = 1
                j = j + 1
            i = i + 1
        return view

    def get_full_state(self, char):
        obs = [np.array(self.__state_of_agent(char)),
               np.array(self.__state_of_board(char)),
               np.array(self.__agent_layer(char, allies=True)),
               np.array(self.__agent_layer(char, allies=False))]
        return np.asarray(obs)

    # for simplicity, use same size as playfield
    # normalise all values
    def __state_of_agent(self, char):
        agent_state_layer = np.zeros((self.size, self.size), dtype=np.float16)
        agent_state_layer[0][0] = char.x / self.size
        agent_state_layer[0][self.size-1] = char.y / self.size
        agent_state_layer[self.size-1][0] = char.health / char.max_health
        # agent_state_layer[self.size-1][self.size-1] = 0
        return self.__add_boarder(agent_state_layer, 0)

    def __state_of_board(self, char):
        state_layer = np.zeros((self.size, self.size), dtype=np.uint8)
        if not char.defeated:
            state_layer[char.x][char.y] = 1
        return self.__add_boarder(state_layer, 1)

    # create layer for allies or enemies of agent
    def __agent_layer(self, char, allies):
        agent_layer = np.zeros((self.size, self.size), dtype=np.uint8)
        for agent in self.agents:
            if not agent.defeated and char != agent:
                if allies:
                    if char.team == agent.team:
                        agent_layer[agent.x][agent.y] = 1
                if not allies:
                    if char.team != agent.team:
                        agent_layer[agent.x][agent.y] = 1
        return self.__add_boarder(agent_layer, 0)

    def __add_boarder(self, array, value):
        horizontal = np.full(self.size, value)
        array = np.insert(array, self.size, horizontal, 0)
        array = np.insert(array, 0, horizontal, 0)
        vertical = np.full((1, self.size + 2), value)
        array = np.insert(array, self.size, vertical, 1)
        array = np.insert(array, 0, vertical, 1)
        return array

    ### CREATE AGENTS DEPENDING ON SCENARIO ###

    def create_agents(self, scenario):
        for char_type in scenario.involved_agents:
            positions = scenario.involved_agents[char_type]
            for pos in positions:
                if char_type == "warrior":
                    Warrior(x=pos[0], y=pos[1], environment=self)
                elif char_type == "hero-dummy":
                    HeroDummyAgent(x=pos[0], y=pos[1], environment=self)
                elif char_type == "goblin-dummy":
                    GoblinDummyAgent(x=pos[0], y=pos[1], environment=self)
                elif char_type == "goblin":
                    Goblin(x=pos[0], y=pos[1], environment=self)
                elif char_type == "good_goblin":
                    GoodGoblin(x=pos[0], y=pos[1], environment=self)
                elif char_type == "goblin_ii":
                    Goblin_ii(x=pos[0], y=pos[1], environment=self)
                elif char_type == "good_goblin_ii":
                    GoodGoblin_ii(x=pos[0], y=pos[1], environment=self)
                else:
                    raise Exception("Not implemented error for " + char_type)

