from Scenarios.Scenario import Scenario

import torch.optim as optim
from Helper.DQN import DQN
from Helper.ReplayBuffer import ReplayBuffer


class DummyEnv(Scenario):
    def __init__(self):
        super().__init__()
        self.scenario_name = "DummyEnv"

        # dict of agents, with list of tuples (x and y coordinates)
        self.involved_agents = {"warrior": [(0, 2)], "goblin-dummy": [(4, 2)]}
        self.agents_with_dqn = ["warrior"]
        self.reward_dict = {"warrior": []}

        self.net = {"warrior": DQN((2, 5, 5), 8).to(self.cfg.device)}
        self.tgt_net = {"warrior": DQN((2, 5, 5), 8).to(self.cfg.device)}
        self.buffer = {"warrior": ReplayBuffer(10000)}

        self.optimizer = {"warrior": optim.Adam(self.net["warrior"].parameters(), lr=self.cfg.dqn_LEARNING_RATE)}

    def save_rewards(self, environment):
        for char in environment.agents:
            if char.class_name in self.agents_with_dqn:
                self.reward_dict[char.class_name].append(sum(char.rewards))
                print(str(char.class_name) + ": " + str(sum(char.rewards)))
