from Scenarios.Scenario import Scenario

import torch.optim as optim
from Helper.DQN import DQN
from Helper.ReplayBuffer import ReplayBuffer


class Gob2VsGob2Env(Scenario):
    def __init__(self):
        super().__init__()
        self.scenario_name = "2GobVs2GobEnv"

        # dict of agents, with list of tuples (x and y coordinates)
        self.involved_agents = {"good_goblin": [(0, 1)], "good_goblin_ii": [(0, 3)], "goblin": [(4, 1)], "goblin_ii": [(4, 3)]}
        self.agents_with_dqn = ["good_goblin", "good_goblin_ii", "goblin", "goblin_ii"]
        self.reward_dict = {"good_goblin": [], "good_goblin_ii": [], "goblin": [], "goblin_ii": []}

        if not self.cfg.fully_observable:
            dqn_input_shape = (2, 5, 5)
        else:
            dqn_input_shape = (4, self.cfg.pf_dim+2, self.cfg.pf_dim+2)

        self.net = {"good_goblin": DQN(dqn_input_shape, 8).to(self.cfg.device),
                    "good_goblin_ii": DQN(dqn_input_shape, 8).to(self.cfg.device),
                    "goblin": DQN(dqn_input_shape, 8).to(self.cfg.device),
                    "goblin_ii": DQN(dqn_input_shape, 8).to(self.cfg.device)}
        self.tgt_net = {"good_goblin": DQN(dqn_input_shape, 8).to(self.cfg.device),
                        "good_goblin_ii": DQN(dqn_input_shape, 8).to(self.cfg.device),
                        "goblin": DQN(dqn_input_shape, 8).to(self.cfg.device),
                        "goblin_ii": DQN(dqn_input_shape, 8).to(self.cfg.device)}
        self.buffer = {"good_goblin": ReplayBuffer(10000),
                       "good_goblin_ii": ReplayBuffer(10000),
                       "goblin": ReplayBuffer(10000),
                       "goblin_ii": ReplayBuffer(10000)}

        lr = self.cfg.dqn_LEARNING_RATE
        self.optimizer = {"good_goblin": optim.Adam(self.net["good_goblin"].parameters(), lr=lr),
                          "good_goblin_ii": optim.Adam(self.net["good_goblin_ii"].parameters(), lr=lr),
                          "goblin": optim.Adam(self.net["goblin"].parameters(), lr=lr),
                          "goblin_ii": optim.Adam(self.net["goblin_ii"].parameters(), lr=lr)}

    def save_rewards(self, environment):
        for char in environment.agents:
            if char.class_name in self.agents_with_dqn:
                self.reward_dict[char.class_name].append(sum(char.rewards))
                print(str(char.class_name) + ": " + str(sum(char.rewards)))