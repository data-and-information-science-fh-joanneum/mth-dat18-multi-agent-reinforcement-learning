from Helper.Config import Config


class Scenario:
    def __init__(self):
        self.cfg = Config
        self.involved_agents = []
        self.round_count_list = []
        self.team_win_count = {1: 0, 2: 0}
