from Agents.Char import Char


# base class for ranged chars (cleric and mage)
class RangedFighter(Char):
    def __init__(self, x, y, environment):
        super().__init__(x, y, environment)

    # function to find targets of area of effect [AOE] actions
    def find_targets(self, dir, x_list, y_list):
        self.environment.logger.p(str(self.ID) + " (" + self.class_name + ") action " + dir)
        targets = []
        # iterate over fields with potential targets
        for i in range(len(x_list)):
            if not self.environment.is_field_on_board(x_list[i], y_list[i]):
                continue
            elif self.environment.is_field_empty(x_list[i], y_list[i]):
                continue
            elif self.environment.is_obstacle(x_list[i], y_list[i]):
                continue
            else:
                targets.append(self.environment.board[x_list[i]][y_list[i]])
        # return list of valid targets
        return targets
