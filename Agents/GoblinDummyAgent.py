from Agents.Char import Char


class GoblinDummyAgent(Char):
    def __init__(self, x, y, environment):
        super().__init__(x, y, environment)
        self.armour = 0
        self.health = 20
        self.max_health = self.health
        self.base_dmg = 0
        self.team = 2
        self.environment.goblins.append(self)
        self.class_name = "goblin-dummy"
        self.cn = "d"
        self.actionspace = 4
