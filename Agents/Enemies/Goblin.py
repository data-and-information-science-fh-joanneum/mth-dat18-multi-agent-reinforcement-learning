from Agents.MeleeFighter import MeleeFighter


class Goblin(MeleeFighter):
    def __init__(self, x, y, environment):
        super().__init__(x, y, environment)
        self.armour = 5  #6
        self.health = 15  #18
        self.max_health = self.health
        self.base_dmg = 6
        self.team = 2
        self.environment.goblins.append(self)
        self.class_name = "goblin"
        self.cn = "g"

