from Agents.Char import Char


class HeroDummyAgent(Char):
    def __init__(self, x, y, environment):
        super().__init__(x, y, environment)
        self.armour = 0
        self.health = 20
        self.max_health = self.health
        self.base_dmg = 0
        self.team = 1
        self.environment.heroes.append(self)
        self.class_name = "hero-dummy"
        self.cn = "D"
        self.actionspace = 4
