import random
import torch
import numpy as np
from Helper.Config import Config


# char is the base class for all chars and provides common functions
class Char:
    def __init__(self, x, y, environment):
        # x coordinate
        self.x = x
        # x coordinate
        self.y = y
        # health of char, set in each child constructor.
        self.health = 0
        # bool if char is defeated
        self.defeated = False
        # reference to the playfield
        self.environment = environment
        # unique id managed by the playfield
        self.ID = environment.id_counter
        environment.id_counter = environment.id_counter + 1
        # place char on the playfield
        self.environment.set_char(self)
        # append char to the list of chars
        self.environment.agents.append(self)
        # configuration
        self.cfg = Config
        # list of rewards
        self.rewards = []
        # number of available actions
        self.actionspace = 8
        # reward collected last round
        self.reward_last_round = None
        # observation from last round
        self.observation_last_round = None
        # action chosen last round
        self.action_last_round = None
        # check if died since last round
        self.more_than_one_round_dead = False

    ### DAMAGE & HEALING ###

    # function for dealing damage (dmg) to an other char (target)
    def deal_dmg(self, target, dmg):
        self.environment.logger.p(str(self.ID) + " tries to hit " + str(target.ID) + " for " + str(dmg) + " damage")
        # dice roll to determine if target is hit [attack roll]
        hit_dice = random.randint(1, 20)
        self.environment.logger.p("hit dice rolled " + str(hit_dice))
        # check if target is armoured enough to withstand attack [armour class]
        if hit_dice > target.armour or self.cfg.skip_armour_chk:
            if not self.cfg.skip_armour_chk:
                self.environment.logger.p("hit dice > armour class")
            # if dice roll was 20 the dealt damage is increased [critical hit]
            if hit_dice == 20 and self.cfg.allow_crit_hit:
                if self.cfg.allow_crit_hit:
                    self.environment.logger.p("critical hit - damage increased")
                    # damage is only increased by 2 in order to make critical hits less game dominating
                    dmg += self.cfg.crit_modifier
            self.environment.logger.p(str(self.ID) + " hits " + str(target.ID) + " for " + str(dmg) + " damage")
            # reward is added if enemy is hit, reward is subtracted if ally is hit
            if self.environment.is_ally(self, target):
                self.environment.turn_reward += self.cfg.punish_hit_friendly
            else:
                self.environment.turn_reward += self.cfg.reward_hit_enemy
            target.__get_dmg(dmg)
            # dealing damage was successful
            return True
        else:
            self.environment.logger.p("hit dice <= armour class, no damage")
            # dealing damage was not successful
            return False

    # function for a char to get damage
    def __get_dmg(self, dmg):
        self.environment.logger.p(str(self.ID) + " loses " + str(dmg) + " health")
        self.health -= dmg
        # check if char is defeated
        if self.health <= 0:
            self.environment.logger.p(str(self.ID) + " has no health left")
            self.environment.remove_char(self)
        else:
            self.environment.logger.p(str(self.ID) + " has " + str(self.health) + " health left")

    # function for a char to receive healing
    def get_healing(self, heal):
        self.environment.logger.p(str(self.ID) + " is healed for " + str(heal))
        # char can not be healed beyond maximum health
        if self.health == self.max_health:
            self.environment.logger.p(str(self.ID) + " is already at full health")
            # healing was not successful
            return False
        elif self.health + heal >= self.max_health:
            # reward is added for healed health
            self.environment.turn_reward += (self.max_health - self.health)
            # actual healing
            self.health = self.max_health
            self.environment.logger.p(str(self.ID) + " healed to full health")
            # healing was partially successful
            return True
        else:
            # reward is added for healed health
            self.environment.turn_reward += heal
            # actual healing
            self.health += heal
            self.environment.logger.p(str(self.ID) + " healed for " + str(heal) + ", now at " + str(self.health))
            # healing was successful
            return True

    ### MOVING ###
    # moving is handled by the playfield for easy synchronization

    def move_north(self):
        success = self.environment.move_north(self)
        self.__calc_move_rew(success)
        return success

    def move_south(self):
        success = self.environment.move_south(self)
        self.__calc_move_rew(success)
        return success

    def move_east(self):
        success = self.environment.move_east(self)
        self.__calc_move_rew(success)
        return success

    def move_west(self):
        success = self.environment.move_west(self)
        self.__calc_move_rew(success)
        return success

    def __calc_move_rew(self, success):
        if not success:
            self.environment.turn_reward += self.cfg.punish_move

    ### ACTIONS ###

    def choose_random_action(self):
        action = random.randint(0, self.actionspace - 1)
        self.execute_action(action)
        return action

    def choose_dqn_action(self, observation, epsilon, cfg, net):
        # return random floats in the half-open interval [0.0, 1.0)
        rand = np.random.random()
        self.environment.logger.p("random nr: " + str(rand))
        self.environment.logger.p("epsilon: " + str(epsilon))
        if rand < epsilon:
            action = self.choose_random_action()
            self.environment.logger.p("random action: " + str(action))
            return action
        else:
            state_a = observation
            state_v = torch.tensor(np.expand_dims(state_a, axis=0)).to(cfg.device)
            q_vals_v = net(state_v.type(torch.cuda.FloatTensor))
            _, act_v = torch.max(q_vals_v, dim=1)
            action = int(act_v.item())
            self.environment.logger.p("dqn chose: " + str(action))
            self.execute_action(action)
            return action

    def execute_action(self, action):
        if action == 0: self.move_north()
        if action == 1: self.move_east()
        if action == 2: self.move_south()
        if action == 3: self.move_west()
        if action == 4: self.action_north()
        if action == 5: self.action_east()
        if action == 6: self.action_south()
        if action == 7: self.action_west()
