from Agents.MeleeFighter import MeleeFighter


class Warrior(MeleeFighter):
    def __init__(self, x, y, environment):
        super().__init__(x, y, environment)
        self.armour = 8
        self.health = 20
        self.max_health = self.health
        self.base_dmg = 4
        self.team = 1
        self.environment.heroes.append(self)
        self.class_name = "warrior"
        self.cn = "w"
