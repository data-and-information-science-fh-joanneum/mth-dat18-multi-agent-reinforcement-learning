from Agents.MeleeFighter import MeleeFighter


class GoodGoblin_ii(MeleeFighter):
    def __init__(self, x, y, environment):
        super().__init__(x, y, environment)
        self.armour = 5  #6
        self.health = 15  #18
        self.max_health = self.health
        self.base_dmg = 6
        self.team = 1
        self.environment.heroes.append(self)
        self.class_name = "good_goblin_ii"
        self.cn = "o"

