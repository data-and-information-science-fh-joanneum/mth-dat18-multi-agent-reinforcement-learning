from Agents.RangedFighter import RangedFighter


class Mage(RangedFighter):
    def __init__(self, x, y, environment):
        super().__init__(x, y, environment)
        self.armour = 4
        self.health = 10
        self.max_health = self.health
        self.base_dmg = 5
        self.team = 1
        self.environment.heroes.append(self)
        self.class_name = "mage"
        self.cn = "m"

    # function for action in any direction
    def __action(self, dir, x_list, y_list):
        targets = self.find_targets(dir, x_list, y_list)
        if len(targets) == 0:
            self.environment.logger.px("no valid targets")
            return False
        else:
            successes = []
            # mages deal damage to enemies and allies
            for target in targets:
                # check if damage was dealt
                hit = self.deal_dmg(target, self.base_dmg)
                if hit and not self.environment.is_ally(self, target):
                    self.environment.logger.p("hit an non-ally")
                    successes.append(True)
                elif hit:
                    self.environment.logger.p("hit an ally (friendly fire)")
                    successes.append(False)
                else:
                    successes.append(False)
            if True in successes:
                self.environment.logger.px("at least one enemy hit")
                return True
            else:
                self.environment.logger.px("all enemies missed or only friendly fire")
                return False

    # defining the fields effected by a mage action
    # for action north, the area looks like this:
    #[x][x][x]
    #[_][x][_]
    #[_][o][_]
    # where o is the mage, x are fields effected and _ are not effected fields
    def action_north(self):
        x_list = [self.x - 1, self.x - 2, self.x - 2, self.x - 2]
        y_list = [self.y, self.y - 1, self.y, self.y + 1]
        return self.__action("north", x_list, y_list)

    def action_south(self):
        x_list = [self.x + 1, self.x + 2, self.x + 2, self.x + 2]
        y_list = [self.y, self.y - 1, self.y, self.y + 1]
        return self.__action("south", x_list, y_list)

    def action_east(self):
        x_list = [self.x, self.x -1, self.x, self.x + 1]
        y_list = [self.y + 1, self.y + 2, self.y + 2, self.y + 2]
        return self.__action("east", x_list, y_list)

    def action_west(self):
        x_list = [self.x, self.x -1, self.x, self.x + 1]
        y_list = [self.y - 1, self.y - 2, self.y - 2, self.y - 2]
        return self.__action("west", x_list, y_list)
