from Agents.MeleeFighter import MeleeFighter


class Rogue(MeleeFighter):
    def __init__(self, x, y, environment):
        super().__init__(x, y, environment)
        self.armour = 4
        self.health = 12
        self.max_health = self.health
        self.base_dmg = 8
        self.team = 1
        self.environment.heroes.append(self)
        self.class_name = "rogue"
        self.cn = "r"
