from Agents.RangedFighter import RangedFighter


class Cleric(RangedFighter):
    def __init__(self, x, y, environment):
        super().__init__(x, y, environment)
        self.armour = 6
        self.health = 15
        self.max_health = self.health
        self.base_dmg = 2
        self.team = 1
        self.environment.heroes.append(self)
        self.class_name = "cleric"
        self.cn = "c"

    # function for action in any direction
    def __action(self, dir, x_list, y_list):
        targets = self.find_targets(dir, x_list, y_list)
        if len(targets) == 0:
            self.environment.logger.px("no valid targets")
            return False
        else:
            # valid targets have been found, check if action is successful
            successes = []
            for target in targets:
                # clerics heal allies
                if self.environment.is_ally(self, target):
                    success = target.get_healing(self.base_dmg)
                    successes.append(success)
                # clerics deal damage to enemies
                else:
                    success = self.deal_dmg(target, self.base_dmg)
                    successes.append(success)
            # check if at least one action was successful
            if True in successes:
                self.environment.logger.px("at least one enemy hit or ally healed")
                # action was successful
                return True
            else:
                self.environment.logger.px("all enemies missed and no one healed")
                # action was not successful
                return False

    # defining the fields effected by a cleric action
    # for action north, the area looks like this:
    #[x][x][x]
    #[x][_][x]
    #[_][o][_]
    # where o is the cleric, x are fields effected and _ are not effected fields
    def action_north(self):
        x_list = [self.x - 1, self.x - 1, self.x - 2, self.x - 2, self.x - 2]
        y_list = [self.y - 1, self.y + 1, self.y - 1, self.y, self.y + 1]
        return self.__action("north", x_list, y_list)

    def action_south(self):
        x_list = [self.x + 1, self.x + 1, self.x + 2, self.x + 2, self.x + 2]
        y_list = [self.y - 1, self.y + 1, self.y - 1, self.y, self.y + 1]
        return self.__action("south", x_list, y_list)

    def action_east(self):
        x_list = [self.x - 1, self.x + 1, self.x - 1, self.x, self.x + 1]
        y_list = [self.y + 1, self.y + 1, self.y + 2, self.y + 2, self.y + 2]
        return self.__action("east", x_list, y_list)

    def action_west(self):
        x_list = [self.x - 1, self.x + 1, self.x - 1, self.x, self.x + 1]
        y_list = [self.y - 1, self.y - 1, self.y - 2, self.y - 2, self.y - 2]
        return self.__action("west", x_list, y_list)
