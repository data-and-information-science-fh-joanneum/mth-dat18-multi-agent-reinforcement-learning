from Agents.Char import Char


# base class for melee chars (rogue, warrior and goblin)
class MeleeFighter(Char):
    def __init__(self, x, y, environment):
        super().__init__(x, y, environment)

    # function for action in any direction
    def __action(self, dir, target_x, target_y):
        self.environment.logger.p(str(self.ID) + " (" + self.class_name + ") action " + dir)
        # check if target field is valid
        if not self.environment.is_field_on_board(target_x, target_y):
            success = False
        elif self.environment.is_field_empty(target_x, target_y):
            success = False
        elif self.environment.is_obstacle(target_x, target_y):
            success = False
        # check if target char is ally. melee chars can not deal damage to team mates
        elif self.environment.is_ally(self, self.environment.board[target_x][target_y]):
            success = False
        # if target is an enemy, try to deal damage
        else:
            success = self.deal_dmg(self.environment.board[target_x][target_y], self.base_dmg)
        if success:
            self.environment.logger.px("action successful")
        else:
            # TODO implement negative reward for failed actions for other classes
            # (for thesis ok, as goblin and warrior are based on MeleeFighter)
            self.environment.turn_reward += self.cfg.punish_att_fail_mf
            self.environment.logger.px("action failed")
        return success

    def action_north(self):
        return self.__action("north", self.x - 1, self.y)

    def action_south(self):
        return self.__action("south", self.x + 1, self.y)

    def action_east(self):
        return self.__action("east", self.x, self.y + 1)

    def action_west(self):
        return self.__action("west", self.x, self.y - 1)
